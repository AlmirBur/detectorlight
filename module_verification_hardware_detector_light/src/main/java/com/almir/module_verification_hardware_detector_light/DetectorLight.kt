package com.almir.module_verification_hardware_detector_light

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager

data class MinMaxParam(val minValue: Float, val maxValue: Float)

class DetectorLight (context: Context, private val listener: OnOutLightValuesListener, private val param: MinMaxParam)
        : SensorEventListener {
    private var sensorManager: SensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    private var sensorLight: Sensor? = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
    private var currentLight = 0f

    fun sensorIsAvailable(): Boolean = sensorLight != null

    fun startDetecting() {
        if (sensorIsAvailable())
            sensorManager.registerListener(this, sensorLight, SensorManager.SENSOR_DELAY_NORMAL)
    }

    fun stopDetecting() {
        if (sensorIsAvailable())
            sensorManager.unregisterListener(this, sensorLight)
    }

    fun getCurrentValue(): Float = currentLight

    override fun onSensorChanged(event: SensorEvent?) {
        if (event == null) return
        currentLight = event.values[0]
        if (currentLight < param.minValue) listener.onOutMin()
        if (currentLight > param.maxValue) listener.onOutMax()
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }

    interface OnOutLightValuesListener {
        fun onOutMin()
        fun onOutMax()
    }
}