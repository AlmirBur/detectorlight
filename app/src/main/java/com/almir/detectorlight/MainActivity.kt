package com.almir.detectorlight

import android.hardware.camera2.CameraDevice
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.almir.module_verification_hardware_detector_light.DetectorLight
import com.almir.module_verification_hardware_detector_light.MinMaxParam
import com.almir.module_verification_hardware_detector_noise.DetectorNoise
import com.almir.module_verification_hardware_detector_noise.ThresholdParam

class MainActivity : AppCompatActivity() {
    lateinit var detectorLight: DetectorLight
    lateinit var detectorNoise: DetectorNoise

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initDetectorLight()
        initDetectorNoise()
    }

    private fun initDetectorNoise() {
        val noiseText = findViewById<TextView>(R.id.noiseText)
        val noiseButton = findViewById<TextView>(R.id.noiseButton)
        val listener = object : DetectorNoise.OnOutNoiseListener {
            override fun onOutNoise() {
                Toast.makeText(applicationContext, "TOO NOISY", Toast.LENGTH_SHORT).show()
                setText(noiseText, detectorNoise.getCurrentAmplitude().toString())
            }
        }
        detectorNoise = DetectorNoise(listener, ThresholdParam(10000))
        noiseButton.setOnClickListener{
            setText(noiseText, detectorNoise.getCurrentAmplitude().toString())
        }
    }

    private fun initDetectorLight() {
        val lightText = findViewById<TextView>(R.id.lightText)
        val lightButton = findViewById<Button>(R.id.lightButton)
        val listener = object : DetectorLight.OnOutLightValuesListener {
            override fun onOutMin() {
                Toast.makeText(applicationContext, "TOO DARK", Toast.LENGTH_SHORT).show()
                setText(lightText, detectorLight.getCurrentValue().toString())
            }
            override fun onOutMax() {
                Toast.makeText(applicationContext, "TOO LIGHT", Toast.LENGTH_SHORT).show()
                setText(lightText, detectorLight.getCurrentValue().toString())
            }
        }
        detectorLight = DetectorLight(applicationContext, listener, MinMaxParam(20f, 5000f))
        lightButton.setOnClickListener {
            setText(lightText, detectorLight.getCurrentValue().toString())
        }
    }

    private fun setText(textView: TextView, text: String) {
        textView.text = text
    }

    override fun onStart() {
        super.onStart()
        detectorLight.startDetecting()
        detectorNoise.start()

    }

    override fun onStop() {
        super.onStop()
        detectorLight.stopDetecting()
        detectorNoise.stop()
    }
}