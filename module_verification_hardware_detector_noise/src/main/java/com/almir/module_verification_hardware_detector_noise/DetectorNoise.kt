package com.almir.module_verification_hardware_detector_noise

import android.media.MediaRecorder
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

data class ThresholdParam(val threshold: Int)

class DetectorNoise(private val listener: OnOutNoiseListener, private val param: ThresholdParam) {
    private var mediaRecorder: MediaRecorder? = null
    private var refresher: Observable<Int>? = null
    private var disposer: Disposable? = null
    private var currentAmplitude = 0

    fun start() {
        if (mediaRecorder == null) {
            mediaRecorder = MediaRecorder()
            mediaRecorder?.let {
                try {
                    it.setAudioSource(MediaRecorder.AudioSource.MIC)
                    it.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
                    it.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
                    it.setOutputFile("dev/null")
                    it.prepare()
                    it.start()
                } catch (e: Exception) {
                }
            }
        }
        if (refresher == null) {
            refresher = Observable.interval(200, TimeUnit.MILLISECONDS)
                .map {
                    refreshCurrentAmplitude()
                    getCurrentAmplitude()
                }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { if (it > param.threshold) listener.onOutNoise() }
            disposer = refresher?.subscribe()
        }
    }

    fun stop() {
        disposer?.dispose()
        disposer = null
        refresher = null
        mediaRecorder?.let {
            it.stop()
            it.release()
            mediaRecorder = null
        }
    }

    fun getCurrentAmplitude() = currentAmplitude

    private fun refreshCurrentAmplitude() {
        currentAmplitude = if (mediaRecorder != null) mediaRecorder!!.maxAmplitude else 0
    }

    interface OnOutNoiseListener {
        fun onOutNoise()
    }
}