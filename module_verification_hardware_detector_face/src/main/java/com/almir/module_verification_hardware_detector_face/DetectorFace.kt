package com.almir.module_verification_hardware_detector_face

import android.app.Activity
import android.content.Context
import android.graphics.*
import android.hardware.Camera
import com.google.firebase.FirebaseApp
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import io.reactivex.subjects.PublishSubject

data class BoundsParam(val left: Int, val top: Int, val right: Int, val bottom: Int)

class DetectorFace(private val context: Context,
                   private val listener: OnFaceExistListener,
                   private val camera: Camera,
                   param: BoundsParam
) {
    private var detector: FirebaseVisionFaceDetector? = null
    private var imageProvider: PublishSubject<ByteArray>? = null
    private var disposer: Disposable? = null
    private var bounds = Rect(param.left, param.top, param.right, param.bottom)
    private var faceIsInBounds = false

    private var metadata: FirebaseVisionImageMetadata? = null
    private var rotation = ROTATION_270
    private var reducedImageWidth = 320
    private var reducedImageHeight = 240
    private var imageWidth = 320
    private var imageHeight = 240
    private var ratio = 1
    private val rate = 6

    private fun reduceResolutionYUV420(data: ByteArray): ByteArray {
        val reducedImage = ByteArray(reducedImageWidth * reducedImageHeight * 3 / 2)
        var i = 0
        for (y in 0 until reducedImageHeight * ratio step ratio) {
            for (x in 0 until reducedImageWidth * ratio step ratio) {
                reducedImage[i] = data[y * imageWidth + x]
                i++
            }
        }
        val product = imageWidth * imageHeight
        for (y in 0 until reducedImageHeight * ratio / 2 step ratio) {
            for (x in 0 until reducedImageWidth * ratio step 2 * ratio) {
                reducedImage[i] = data[product + (y * imageWidth) + x]
                i++
                reducedImage[i] = data[product + (y * imageWidth) + (x + 1)]
                i++
            }
        }
        return reducedImage
    }

    private fun getCameraId(facing: Int): Int {
        val cameraCount = Camera.getNumberOfCameras()
        val cameraInfo = Camera.CameraInfo()
        for (i in 0 until cameraCount) {
            Camera.getCameraInfo(i, cameraInfo)
            if (cameraInfo.facing == facing) {
                return i
            }
        }
        return -1
    }

    //Взял метод из класса CameraController. Закоментировал одни строку, чтобы заработало на моём устройстве
    private fun getDisplayOrientation(): Int {
        val cameraInfo = Camera.CameraInfo()
        Camera.getCameraInfo(getCameraId(Camera.CameraInfo.CAMERA_FACING_FRONT), cameraInfo)
        val rotation = (context as Activity).windowManager.defaultDisplay.rotation
        val degrees = when (rotation) {
            ROTATION_0 -> 0
            ROTATION_90 -> 90
            ROTATION_180 -> 180
            ROTATION_270 -> 270
            else -> 90
        }
        var displayOrientation: Int
        if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            displayOrientation = (cameraInfo.orientation + degrees) % 360
            //displayOrientation = (360 - displayOrientation) % 360
        } else {
            displayOrientation = (cameraInfo.orientation - degrees + 360) % 360
        }
        return displayOrientation
    }

    private fun initParams() {
        rotation = getDisplayOrientation() / 90
        ratio = Math.ceil(camera.parameters.previewSize.width / NORMAL_IMAGE_WIDTH - 0.001).toInt()
        imageWidth = camera.parameters.previewSize.width
        imageHeight = camera.parameters.previewSize.height
        reducedImageWidth = imageWidth / ratio
        reducedImageWidth -= reducedImageWidth % 2
        reducedImageHeight = imageHeight / ratio
        reducedImageHeight -= reducedImageHeight % 2
        metadata = FirebaseVisionImageMetadata.Builder()
            .setWidth(reducedImageWidth)
            .setHeight(reducedImageHeight)
            .setRotation(rotation)
            .setFormat(camera.parameters.previewFormat)
            .build()
    }

    fun start() {
        if (detector == null) {
            FirebaseApp.initializeApp(context)
            val realTimeOptions = FirebaseVisionFaceDetectorOptions.Builder().build()
            detector = FirebaseVision.getInstance().getVisionFaceDetector(realTimeOptions)
            initParams()
        }
        if (imageProvider == null) {
            imageProvider = PublishSubject.create<ByteArray>()
            disposer = imageProvider
                ?.subscribeOn(Schedulers.computation())
                ?.observeOn(Schedulers.computation())
                ?.map {
                    val reducedImage = reduceResolutionYUV420(it)
                    FirebaseVisionImage.fromByteArray(reducedImage, metadata!!)
                }
                ?.doOnNext {
                    detector?.detectInImage(it)
                        ?.addOnSuccessListener { faces ->
                            if (faces.isEmpty()) {
                                faceIsInBounds = false
                                listener.faceExist(false)
                            } else {//если лиц больше одного?
                                faceIsInBounds = boundsContain(faces.first().boundingBox)
                                listener.faceExist(true)
                            }
                        }
                        ?.addOnFailureListener {
                            faceIsInBounds = false
                            listener.faceExist(false)
                        }
                }
                ?.subscribe()
            var counter = 0
            camera.setPreviewCallback { p0, _ ->
                if (counter == 0) imageProvider?.onNext(p0)
                counter++
                if (counter >= rate) counter = 0
            }
        }
    }

    private fun boundsContain(rect: Rect): Boolean {
        val newRect = when (rotation) {
            ROTATION_0 -> {
                Rect(imageWidth - rect.right * ratio, rect.top * ratio,
                    imageWidth - rect.left * ratio, rect.bottom * ratio)
            }
            ROTATION_90 -> {
                Rect(imageHeight - rect.right * ratio, rect.top * ratio,
                    imageHeight - rect.left * ratio, rect.bottom * ratio)
            }
            ROTATION_180 -> {
                Rect(imageWidth - rect.right * ratio, rect.top * ratio,
                    imageWidth - rect.left * ratio, rect.bottom * ratio)
            }
            ROTATION_270 -> {
                Rect(imageHeight - rect.right * ratio, rect.top * ratio,
                    imageHeight - rect.left * ratio, rect.bottom * ratio)
            }
            else -> rect
        }
        return bounds.contains(newRect)
    }

    fun stop() {
        disposer?.dispose()
        disposer = null
        imageProvider = null
        detector?.close()
        detector = null
    }

    fun faceIsInBounds(): Boolean = faceIsInBounds

    interface OnFaceExistListener {
        fun faceExist(value: Boolean)
    }

    companion object {
        private const val ROTATION_0 = 0
        private const val ROTATION_90 = 1
        private const val ROTATION_180 = 2
        private const val ROTATION_270 = 3

        //До какой ширины сжимать изображение
        private const val NORMAL_IMAGE_WIDTH = 100.0
    }
}